# FROM --platform=linux/amd64 php:8.1.18-fpm
ARG ALPINE_VERSION=3.16
FROM alpine:${ALPINE_VERSION}


# Set the working directory in the container
WORKDIR /var/www/html

# Update the package lists and install necessary dependencies
RUN apk add --no-cache \
  git \
  curl \
  tini \
  nginx \
  php81 \
  php81-bcmath \
  php81-ctype \
  php81-curl \
  php81-dom \
  php81-fpm \
  php81-ftp \
  php81-fileinfo \
  php81-gd \
  php81-intl \
  php81-json \
  php81-exif \
  php81-mbstring \
  php81-mysqli \
  php81-opcache \
  php81-openssl \
  php81-pdo \
  php81-pdo_mysql \
  php81-pcntl \
  php81-phar \
  php81-posix \
  php81-redis \
  php81-simplexml \
  php81-session \
  php81-tokenizer \
  php81-xml \
  php81-xmlreader \
  php81-xmlwriter \
  php81-zlib \
  php81-zip \
  php81-iconv \
  icu-data-full \
  supervisor \
  bash 

# Install Composer
# Create symlink so programs depending on `php` still function
RUN ln -s /usr/bin/php81 /usr/bin/php

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
     --install-dir=/usr/local/bin --filename=composer

# Copy the default Nginx configuration
COPY /docker/config/nginx/nginx.conf /etc/nginx/nginx.conf

COPY ./*.sh /var/www/html/

COPY ./entrypoint.sh ./entrypoint.sh
RUN chmod +x ./entrypoint.sh

# Configure PHP-FPM
COPY /docker/config/php/fpm-pool.conf /etc/php81/php-fpm.d/www.conf

# Configre php.ini
COPY /docker/config/php/php.ini /etc/php81/conf.d/php.ini

# Configure supervisord
COPY /docker/config/php/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www/html /run /var/lib/nginx /var/log/nginx /var/log/php81

# Switch to use a non-root user from here on
USER nobody

# Add application
COPY --chown=nobody ./ /var/www/html/

# Install composer dependencies
RUN composer install \
    --optimize-autoloader \
    --no-interaction \
    --no-progress \
    --no-scripts \
    --ignore-platform-req=ext-exif \
    --ansi

# Expose ports for Nginx
EXPOSE 8000

STOPSIGNAL SIGTERM

# Let supervisord start nginx & php-fpm
ENTRYPOINT ["/sbin/tini", "--", "bash", "./entrypoint.sh"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8000/fpm-ping
