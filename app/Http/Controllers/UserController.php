<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get users with their most expensive orders as a JSON response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function mostExpensiveOrder(): JsonResponse
    {
        $users = new User();

        $userWithMostExpensiveOrder = $users->usersWithMostExpensiveOrder();

        return response()->json($userWithMostExpensiveOrder);
    }

    /**
     * Retrieve users who have purchased all products and return the result as a JSON response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function usersWithAllProducts(): JsonResponse
    {
        $users = new User();

        $userWithAllProductsPurschased = $users->usersWithAllProductsPurchased();

        return response()->json($userWithAllProductsPurschased);
    }

    /**
     * Retrieve the user with the highest total sales and return the result as a JSON response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userWithHighestSales(): JsonResponse
    {
        $users = new User();

        $userWithHighestSale = $users->userWithHighestSales();

        return response()->json($userWithHighestSale);
    }
}
