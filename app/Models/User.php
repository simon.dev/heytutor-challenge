<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Orders::class);
    }

    /**
     * Get an array of users with their most expensive order.
     *
     * @return array
     */
    public function usersWithMostExpensiveOrder(): array
    {
        $mostExpensiveOrders = User::select('users.*', 'o.product_id', 'o.quantity', 'o.total_amount', 'p.name as product_name', 'p.price')
            ->leftJoinSub(Orders::select('user_id', 'product_id', 'quantity', 'total_amount')
                ->orderByDesc('total_amount'), 'o', function ($join) {
                    $join->on('users.id', '=', 'o.user_id');
                })
            ->leftJoin('products as p', 'o.product_id', '=', 'p.id')
            ->groupBy('users.id', 'o.product_id', 'o.quantity', 'o.total_amount')
            ->orderBy('o.total_amount', 'DESC')
            ->get();
                
        $users = [];
        foreach($mostExpensiveOrders as $user) {
            if($user->product_id) {
                $users[] = [
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email
                    ],
                    'most_expensive_order' => [
                        'product_id' => $user->product_id,
                        'product_name' => $user->product_name,
                        'quantity' => $user->quantity,
                        'total_amount' => $user->total_amount,
                    ]
                ];
            } else {
                $users[] = [
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email
                    ],                    
                    'most_expensive_order' => null
                ];
            }
        }

        return $users;
    }

    /**
     * Get an array of users who has purchase all products.
     *
     * @return array
     */
    public function usersWithAllProductsPurchased(): array
    {

        $usersWithProductsPurchased = User::select('users.*')
            ->whereHas('orders', function ($query) {
                $query->select('user_id')
                    ->join('products', 'orders.product_id', '=', 'products.id')
                    ->groupBy('user_id')
                    ->havingRaw('COUNT(DISTINCT products.id) = ?', [Products::count()]);
            })
            ->get();

        $users = [];
        foreach($usersWithProductsPurchased as $user) {
            if($user->id) {
                $users[] = [
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email
                    ]
                ];
            } else {
                $users[] = [
                    'user' => null
                ];
            }
        }

        return $users;
    }

    /**
     * Retrieve all users with the highest total sales and return the result as an array.
     *
     * @return array
     */
    public function userWithHighestSales(): array
    {
        $userWithHighestSales = User::with(['orders.product'])
            ->select('users.*')
            ->addSelect([
                'total_order_amount' => Orders::selectRaw('SUM(total_amount)')
                    ->whereColumn('orders.user_id', 'users.id')
                    ->where('total_amount', function ($query) {
                        $query->from('orders')
                            ->selectRaw('MAX(total_amount)');
                    })
            ])
            ->whereHas('orders', function ($query) {
                $query->where('total_amount', function ($subQuery) {
                    $subQuery->selectRaw('MAX(total_amount)')
                        ->from('orders');
                });
            })
            ->get();
    
        $users = [];
        foreach($userWithHighestSales as $user) {
            $users[] = [
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email
                ],
                'total_order_amount' => $user->total_order_amount
            ];
        }

        return $users;
    }

}
