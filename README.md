
# Hey tutor - code challenge

In this repo you'll find the solution to the challenge proposed for Senior Software engineer. This is a laravel api with 3 endpoints, it's already dockerized and it's easy to set up and running


## Authors

- [@simon.dev](https://gitlab.com/simon.dev)


## Run Locally

To run this there are a few services that you need to hava previusly installed, those are:

- Docker
- Docker compose

Important notes:
- Please make sure that you don't have any other instance of mysql or other app runing in port 3306 since it's the default mysql port. If that is the case please, stop any service running there or change DB_PORT=3306 value in the .env file to match the corresponding open and free port in your system.

References on how to install it:
- [unix based systems](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)

- [windows](https://linuxhint.com/install-docker-compose-windows/)

Once you have those services installed then you can proceed to run the project using docker compose.

Clone the project

```bash
  git clone https://gitlab.com/simon.dev/heytutor-challenge.git
```

Go to the project directory

```bash
  cd my-project
```

copy the env files

```bash
  cp .env.example .env
```

run docker compose

```bash
  docker-compose up -d
```

verify if thee services are running

```bash
  docker-compose ps
```


## Migration and seeders

This projects has the migration and seeders needed for it to work, those migration and seeders runs automatically when using docker-compose but if you want to run it locally on your host machine just run this:

```bash
php artisan migrate
php artisan db:seed
```
## API Reference

#### Get users with most expensive order

```http
  GET /api/users/most-expensive-order
```

### response example: 

```json
[
    {
        "user": {
            "id": 1,
            "name": "Mr. Jayson Weber MD",
            "email": "kessler.harmony@example.com"
        },
        "most_expensive_order": {
            "product_id": 1,
            "product_name": "ut",
            "quantity": 99,
            "total_amount": "675.13"
        }
    },
    {
        "user": {
            "id": 2,
            "name": "Dr. Marlin Smitham",
            "email": "leif.lakin@example.net"
        },
        "most_expensive_order": {
            "product_id": 2,
            "product_name": "sit",
            "quantity": 5,
            "total_amount": "965.03"
        }
    },
    {
        "user": {
            "id": 3,
            "name": "Friedrich Rosenbaum II",
            "email": "stamm.lizzie@example.com"
        },
        "most_expensive_order": {
            "product_id": 3,
            "product_name": "neque",
            "quantity": 5,
            "total_amount": "250.23"
        }
    },
  ]
```

#### Get user with all products purschased

```http
  GET /api/users/all-products-purschased
```
### response example: 

```json
[
    {
        "user": {
            "id": 5,
            "name": "Yazmin Grant",
            "email": "carmine.waters@example.net"
        },
        "purchased_products_count": 100
    }
]
```

#### Retrieve the user with the highest total sales

```http
  GET /api/users/highest-sales
```

### response example:

```json
{
    "user": {
        "id": 5,
        "name": "Yazmin Grant",
        "email": "carmine.waters@example.net",
        "total_sales": "2761680.81"
    }
}
```



## Deployment

In order to deploy this project just push the code to the repository, the ci/cd will take care of the rest.


## Queries explained

In this seccion I'll exaplin the queries used for all the endpoints

### 1 - Users with their most expensive order:

```sql
SELECT users.*, o.product_id, o.quantity, o.total_amount, p.name AS product_name, p.price
FROM users
LEFT JOIN (
    SELECT user_id, product_id, quantity, total_amount
    FROM orders
    ORDER BY total_amount DESC
) AS o ON users.id = o.user_id
LEFT JOIN products AS p ON o.product_id = p.id
GROUP BY users.id, o.product_id, o.quantity, o.total_amount
ORDER BY o.total_amount DESC;
```

## explanation:
The query retrieves information from the "users" table along with details about their orders and the corresponding products. It combines data from multiple tables through left joins and groups the results based on user and product information. The goal is to gather user details, order specifics (such as product ID, quantity, and total amount), and product information (like product name and price) into a single result set. This allows for a comprehensive view of user orders and the associated product details.

### 2 - Users who has purchase all the product

```sql
SELECT users.*, COUNT(DISTINCT products.id) as purchased_products_count
FROM users
LEFT JOIN orders ON users.id = orders.user_id
LEFT JOIN products ON orders.product_id = products.id
GROUP BY users.id, users.name, users.email
HAVING purchased_products_count = <totalProductsCount>;
```
## explanation:
The query retrieves user details from the "users" table, including their name and email. It calculates the count of distinct product IDs that each user has purchased by joining the "orders" and "products" tables. The query groups the results by user identifiers and filters the results to include only users who have purchased a specific count of distinct products (defined by totalProductsCount). This provides a list of users who have purchased a certain number of different products, along with their details.

### 3 - Users with the highest total sales

```sql
SELECT users.*, 
    (SELECT SUM(total_amount) 
     FROM orders 
     WHERE orders.user_id = users.id 
     AND total_amount = (SELECT MAX(total_amount) FROM orders)) AS total_order_amount
FROM users
WHERE EXISTS (
    SELECT 1 
    FROM orders 
    WHERE users.id = orders.user_id 
    AND total_amount = (SELECT MAX(total_amount) FROM orders)
);
```
## explanation:
The raw SQL query retrieves user details from the users table while calculating the total order amount for each user. Subqueries are used to calculate the sum of total_amount from the orders table, considering orders with the maximum total_amount for each user. The query filters users based on the existence of orders with the highest total_amount, ensuring that only users with such orders are included in the result.
## Infrastructure

This is deployed in a ECS cluster in AWS, you can acces to it using this dns
```url
host: http://lb-heytuto-service-1623689659.us-east-2.elb.amazonaws.com
```
So you can test it right away.

For this I used AWS ECS, RDS, S3 and a couple of IAM users, everyting is autimatically deployed using the gitlab ci and cd pipeline
## Unit test

This project is fully tested, once you have the proyect running with `docker-compose` run this:

```bash
php artisan test
```
This sould run all the test.
## Fine tunning

All php and nginx fine tunning can be found in `/docker/config/php` `/docker/config/nginx`

In resume, as a fine tuniing I enabled opcache and gave them some values, also I regulated the number of min and max php processes running using pm, 