<?php

namespace Tests\Feature;

use App\Models\Orders;
use App\Models\Products;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_users_with_most_expensive_order(): void
    {

        $testUser = User::factory(5)->create();
        $testProduct = Products::factory()->create();
        $totalAmount = 0;

        foreach ($testUser as $user) {
            $totalAmount = 2000 * $user->id;
            $mockOrder = Orders::factory()->create([
                'user_id' => $user->id,
                'product_id' => $testProduct->id,
                'total_amount' => $totalAmount
            ]);
        }

        $testData = [
            [
                "user" => [
                    "id" => $testUser[4]->id,
                    "name" => $testUser[4]->name,
                    "email" => $testUser[4]->email
                ],
                "most_expensive_order" => [
                    "product_id" => $testProduct->id,
                    "product_name" => $testProduct->name,
                    "quantity" => $mockOrder->quantity,
                    "total_amount" => number_format($totalAmount, 2, '.', '')
                ]
            ]
        ];


        $response = $this->get('/api/users/most-expensive-order');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'user' => [
                    'id',
                    'name',
                    'email',
                ],
                'most_expensive_order',
            ]
        ]);
        $this->assertEquals($response[0], $testData[0]);
    }

    public function test_users_who_has_bought_all_products(): void
    {
        $testProducts = Products::factory(50)->create();
        $testUsers = User::factory(5)->create();

        for ($i = 0; $i <= 1; $i++) {
            foreach ($testProducts as $product) {
                Orders::factory()->create([
                    'user_id' => $testUsers[$i]->id,
                    'product_id' => $product->id
                ]);
            }
        }

        $testData = [
            [
                'user' => [
                    'id' => $testUsers[0]->id,
                    'name' => $testUsers[0]->name,
                    'email' => $testUsers[0]->email
                ],
            ],
            [
                'user' => [
                    'id' => $testUsers[1]->id,
                    'name' => $testUsers[1]->name,
                    'email' => $testUsers[1]->email
                ]
            ]
        ];

        $response = $this->get('/api/users/all-products-purschased');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'user' => [
                    'id',
                    'name',
                    'email',
                ],
            ]
        ]);
        $this->assertEquals($response->original, $testData);
    }

    public function test_user_with_highest_sale(): void
    {
        $testUsers = User::factory(20)->create();
        $users = [];

        foreach ($testUsers as $index => $user) {
            if ($index < 3) {
                array_push($users, [
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                    ],
                    'total_order_amount' => 999999.00,
                ]);

                Orders::factory()->create([
                    'user_id' => $user->id,
                    'total_amount' => 999999
                ]);
            } else {
                Orders::factory()->create([
                    'user_id' => $user->id,
                ]);
            }
        }

        $response = $this->get('/api/users/highest-sales');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'user' => [
                    'id',
                    'name',
                    'email',
                ],
                'total_order_amount',
            ]
        ]);
        $this->assertEquals($response->original, $users);
    }
}
