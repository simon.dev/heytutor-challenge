<?php

namespace Database\Seeders;

use App\Models\Orders;
use App\Models\Products;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = Products::all();
        $usersId = [2, 4, 6];

        foreach($usersId as $userId) {
            foreach($products as $product) {
                $orderQuantity = 25;
    
                $order = new Orders();
                $order->user_id = $userId;
                $order->product_id = $product->id;
                $order->quantity = $orderQuantity;
                $order->total_amount = $orderQuantity * $product->price;
                $order->save();
            }
        }
    }
}
