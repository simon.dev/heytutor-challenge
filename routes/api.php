<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/users/most-expensive-order', [UserController::class, 'mostExpensiveOrder']);
Route::get('/users/all-products-purschased', [UserController::class, 'usersWithAllProducts']);
Route::get('/users/highest-sales', [UserController::class, 'userWithHighestSales']);